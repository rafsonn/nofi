import 'package:flutter/material.dart';
import 'package:nofi/config/RoutingConstants.dart';
import 'package:nofi/pages/NotificationsPage.dart';
import 'package:nofi/pages/GroupsPage.dart';
import 'package:nofi/pages/LoginPage.dart';
import 'package:nofi/pages/MainPage.dart';
import 'package:nofi/pages/SettingsPage.dart';

class Router {
  // ignore: missing_return
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case LoginPageRoute:
        return MaterialPageRoute(builder: (context) => LoginPage());
      case MainPageRoute:
        return MaterialPageRoute(builder: (context) => MainPage());
      case GroupsPageRoute:
        return MaterialPageRoute(builder: (context) => GroupsPage());
      case NotificationsPageRoute:
        return MaterialPageRoute(builder: (context) => NotificationsPage(group: settings.arguments));
      case SettingsPageRoute:
        return MaterialPageRoute(builder: (context) => SettingsPage());
      case TabletPageRoute:
        return MaterialPageRoute(builder: (context) => GroupsPage());
    }
  }
}
