import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:nofi/app/Router.dart';
import 'package:nofi/config/RoutingConstants.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:nofi/models/AppLocalizations.dart';

class Nofi extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.light.copyWith(
            statusBarColor: Colors.transparent));
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (builder, orientation) {
        SizeConfig().init(constraints, orientation);
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Nofi',
          theme: ThemeData(
            primaryColor: Colors.white,
          ),
          supportedLocales: [
            Locale('en', 'US'),
            Locale('pl', 'PL')
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          localeResolutionCallback: (locale, supportedLocales) {
            for (var supportedLocale in supportedLocales) {
              if (supportedLocale.languageCode == locale.languageCode &&
                  supportedLocale.countryCode == locale.countryCode) {
                return supportedLocale;
              }
            }
            return supportedLocales.first;
          },
          initialRoute: MainPageRoute,
          onGenerateRoute: Router.generateRoute,
        );
      });
    });
  }
}
