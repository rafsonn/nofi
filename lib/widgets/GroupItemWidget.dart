import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nofi/config/DatabaseService%20.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:provider/provider.dart';

class GroupItemWidget extends StatelessWidget {
  final snapshot;
  final onPressed;
  GroupItemWidget(this.snapshot, this.onPressed);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserAuth>(context);
    final db = DatabaseService();
    var locale = Localizations.localeOf(context).toString();

    return FlatButton(
      onPressed: this.onPressed,
      color: Palette.backgroundColor,
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 0.5),
          child: Column(
            children: <Widget>[
              Divider(
                color: Palette.dividerColor,
                height: 12.0,
              ),
              Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 24.0,
                    backgroundColor: Palette.avatarColor,
                    child: Icon(
                      Icons.group,
                      color: Palette.iconColorLight,
                    ),
                  ),
                  title: Row(
                    children: <Widget>[
                      Expanded(child: Text(snapshot.name)),
                      SizedBox(
                        width: 16.0,
                      ),
                      Text(
                        snapshot.timestamp == 0
                            ? "N/A"
                            : DateFormat.yMMMd(locale).add_jm().format(snapshot.timestamp.toDate()),
                        style: TextStyle(fontSize: SizeConfig.isLargeScreen? null: 1.5 * SizeConfig.textMultiplier),
                        textAlign: TextAlign.right,
                      ),
                    ],
                  ),
                  subtitle: Text(snapshot.lastNotification),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    size: 14.0,
                  ),
                ),
                actions: <Widget>[],
                secondaryActions: <Widget>[
                  snapshot.admin == user.user.uid
                      ? IconSlideAction(
                          caption: AppLocalizations.of(context).translate('Delete'),
                          color: Palette.deleteActionColor,
                          icon: Icons.delete,
                          onTap: () => db.removeGroup(snapshot.uid),
                        )
                      : snapshot.members.contains(user.user.uid)
                          ? IconSlideAction(
                              caption: AppLocalizations.of(context).translate('Exit'),
                              color: Palette.exitActionColor,
                              icon: Icons.close,
                              onTap: () => db.exitGroup(snapshot.uid, user.user),
                            )
                          : IconSlideAction(
                              caption: AppLocalizations.of(context).translate('Join'),
                              color: Palette.joinActionColor,
                              icon: Icons.input,
                              onTap: () => db.joinGroup(snapshot.uid, user.user),
                            )
                ],
              ),
            ],
          )),
    );
  }
}
