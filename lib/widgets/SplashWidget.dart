import 'package:flutter/material.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/widgets/Logo.dart';

class SplashWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        decoration: BoxDecoration(gradient: Palette.backgroundGradient),
        child: Center(child: Logo()));
  }
}
