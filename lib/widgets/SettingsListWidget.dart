import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/models/Validator.dart';
import 'package:provider/provider.dart';

class SettingsListWidget extends StatefulWidget {
  final settingsPageKey;

  SettingsListWidget({Key key, this.settingsPageKey}) : super(key: key);

  @override
  _SettingsListWidgetState createState() => _SettingsListWidgetState();
}

class _SettingsListWidgetState extends State<SettingsListWidget> {
  final GlobalKey<ScaffoldState> _settingsPageKey = GlobalKey();

  final GlobalKey<FormState> _changeEmailKey = GlobalKey();
  final GlobalKey<FormState> _changePasswordKey = GlobalKey();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _oldPasswordController = TextEditingController();
  final TextEditingController _newPasswordController = TextEditingController();

  final _validate = Validator();
  bool _autoValidateEmail;
  bool _autoValidatePassword;

  final BoxDecoration tileDecoration = BoxDecoration(
      color: Palette.whiteBackgroundColor,
      borderRadius: BorderRadius.circular(8.0),
      boxShadow: [
        BoxShadow(color: Palette.shadowColor, offset: Offset(5.0, 5.0), blurRadius: 10.0)
      ]);

  @override
  void initState() {
    setState(() {
      _autoValidateEmail = false;
      _autoValidatePassword = false;
    });
    super.initState();
  }

  @override
  void dispose() {
    _newPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserAuth>(context);
    var x = user.user.email;
    return Scaffold(
      key: _settingsPageKey,
      body: Container(
          padding: EdgeInsets.all(10),
          child: ListView(children: <Widget>[
            /// Change Email Form
            Container(
              decoration: tileDecoration,
              child: ExpansionTile(
                leading: Icon(
                  Icons.alternate_email,
                  color: Palette.iconColor,
                ),
                title: Text(
                  AppLocalizations.of(context).translate('Change_email'),
                  style: TextStyle(color: Palette.textColor),
                ),
                children: <Widget>[
                  ListTile(
                    title: Center(
                        child: Text(user.user.email, style: TextStyle(color: Palette.textColor))),
                  ),
                  Form(
                    key: _changeEmailKey,
                    autovalidate: _autoValidateEmail,
                    child: Column(
                      children: <Widget>[
                        Container(
                            width: SizeConfig.screenWidth,
                            margin:
                                EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                            alignment: Alignment.center,
                            child: TextFormField(
                                controller: _emailController,
                                validator: _validate.email,
                                autovalidate: _autoValidateEmail,
                                keyboardType: TextInputType.emailAddress,
                                textAlign: TextAlign.left,
                                decoration: InputDecoration(
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Palette.underlineColor, width: 0.75)),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Palette.underlineColor, width: 0.5)),
                                  prefixIcon: Icon(Icons.alternate_email, color: Palette.iconColor),
                                  hintText: AppLocalizations.of(context).translate('email'),
                                  hintStyle: TextStyle(color: Palette.textColor),
                                ))),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(
                                left: 10.0 * SizeConfig.widthMultiplier,
                                right: 10.0 * SizeConfig.widthMultiplier,
                                top: 3.8 * SizeConfig.heightMultiplier),
                            alignment: Alignment.center,
                            child: Row(children: <Widget>[
                              Expanded(
                                child: FlatButton(
                                  padding: EdgeInsets.symmetric(vertical: 15.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                  color: Palette.formButtonColor,
                                  onPressed: () async {
                                    setState(() {
                                      _autoValidateEmail = true;
                                    });
                                    if (_changeEmailKey.currentState.validate()) {
                                      if (!await user.changeEmail(_emailController.text)) {
                                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                                        _settingsPageKey.currentState.showSnackBar(SnackBar(
                                          content: Text(AppLocalizations.of(context)
                                                  .translate('Something_goes_wrong') +
                                              ".  " +
                                              AppLocalizations.of(context)
                                                  .translate('Try_to_login_again')),
                                        ));
                                      } else {
                                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                                        _settingsPageKey.currentState.showSnackBar(SnackBar(
                                          content:
                                              Text(AppLocalizations.of(context).translate('Done')),
                                        ));
                                        setState(() {
                                          _autoValidatePassword = false;
                                        });
                                        _emailController.clear();
                                      }
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context).translate('Change'),
                                    style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                                  ),
                                ),
                              ),
                            ])),
                      ],
                    ),
                  ),
                  Divider(height: 10.0)
                ],
              ),
            ),
            Divider(height: 10.0),

            /// Change Password Form
            Container(
              decoration: tileDecoration,
              child: ExpansionTile(
                leading: Icon(
                  Icons.vpn_key,
                  color: Palette.iconColor,
                ),
                title: Text(
                  AppLocalizations.of(context).translate('Change_password'),
                  style: TextStyle(color: Palette.textColor),
                ),
                children: <Widget>[
                  Form(
                    key: _changePasswordKey,
                    autovalidate: _autoValidatePassword,
                    child: Column(
                      children: <Widget>[
                        Container(
                            width: SizeConfig.screenWidth,
                            margin:
                                EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                            alignment: Alignment.center,
                            child: TextFormField(
                                obscureText: true,
                                controller: _oldPasswordController,
                                validator: _validate.isEmpty,
                                autovalidate: _autoValidatePassword,
                                keyboardType: TextInputType.emailAddress,
                                textAlign: TextAlign.left,
                                decoration: InputDecoration(
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Palette.underlineColor, width: 0.75)),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Palette.underlineColor, width: 0.5)),
                                  prefixIcon: Icon(Icons.vpn_key, color: Palette.iconColor),
                                  hintText: AppLocalizations.of(context).translate('old_password'),
                                  hintStyle: TextStyle(color: Palette.textColor),
                                ))),
                        Container(
                            width: SizeConfig.screenWidth,
                            margin:
                                EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                            alignment: Alignment.center,
                            child: TextFormField(
                                obscureText: true,
                                controller: _newPasswordController,
                                validator: _validate.password,
                                autovalidate: _autoValidatePassword,
                                keyboardType: TextInputType.emailAddress,
                                textAlign: TextAlign.left,
                                decoration: InputDecoration(
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Palette.underlineColor, width: 0.75)),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Palette.underlineColor, width: 0.5)),
                                  prefixIcon: Icon(Icons.vpn_key, color: Palette.iconColor),
                                  hintText: AppLocalizations.of(context).translate('new_password'),
                                  hintStyle: TextStyle(color: Palette.textColor),
                                ))),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(
                                left: 10.0 * SizeConfig.widthMultiplier,
                                right: 10.0 * SizeConfig.widthMultiplier,
                                top: 3.8 * SizeConfig.heightMultiplier),
                            alignment: Alignment.center,
                            child: Row(children: <Widget>[
                              Expanded(
                                child: FlatButton(
                                  padding: EdgeInsets.symmetric(vertical: 15.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0)),
                                  color: Palette.formButtonColor,
                                  onPressed: () async {
                                    setState(() {
                                      _autoValidatePassword = true;
                                    });
                                    if (_changePasswordKey.currentState.validate()) {
                                      if (_oldPasswordController.text ==
                                          _newPasswordController.text) {
                                        if (!await user.reauthenticate(
                                            user.user, _oldPasswordController.text)) {
                                          SystemChannels.textInput.invokeMethod('TextInput.hide');
                                          _settingsPageKey.currentState.showSnackBar(SnackBar(
                                            content: Text(AppLocalizations.of(context)
                                                .translate('Something_goes_wrong')),
                                          ));
                                        } else {
                                          if (!await user
                                              .changePassword(_newPasswordController.text)) {
                                            SystemChannels.textInput.invokeMethod('TextInput.hide');
                                            _settingsPageKey.currentState.showSnackBar(SnackBar(
                                              content: Text(AppLocalizations.of(context)
                                                  .translate('Something_goes_wrong')),
                                            ));
                                          } else {
                                            SystemChannels.textInput.invokeMethod('TextInput.hide');
                                            _settingsPageKey.currentState.showSnackBar(SnackBar(
                                              content: Text(
                                                  AppLocalizations.of(context).translate('Done')),
                                            ));
                                            setState(() {
                                              _autoValidatePassword = false;
                                            });
                                            _oldPasswordController.clear();
                                            _newPasswordController.clear();
                                          }
                                        }
                                      } else {
                                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                                        _settingsPageKey.currentState.showSnackBar(SnackBar(
                                          content: Text(AppLocalizations.of(context)
                                              .translate('Passwords_dont_match')),
                                        ));
                                      }
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context).translate('Change'),
                                    style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                                  ),
                                ),
                              ),
                            ])),
                      ],
                    ),
                  ),
                  Divider(height: 10.0)
                ],
              ),
            ),
            Divider(height: 10.0),

            /// Credits
            Container(
              decoration: tileDecoration,
              child: ExpansionTile(
                leading: Icon(
                  Icons.info_outline,
                  color: Palette.iconColor,
                ),
                title: Text(
                  AppLocalizations.of(context).translate('Credits'),
                  style: TextStyle(color: Palette.textColor),
                ),
                children: <Widget>[
                  ListTile(
                      title: Center(
                          child: Text(AppLocalizations.of(context).translate('Developed_by'))),
                      contentPadding: EdgeInsets.all(10)),
                ],
              ),
            ),
          ])),
    );
  }
}
