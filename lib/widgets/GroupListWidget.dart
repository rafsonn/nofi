import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nofi/config/RoutingConstants.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/Group.dart';
import 'package:nofi/widgets/GroupItemWidget.dart';
import 'package:provider/provider.dart';

typedef Null ItemSelectedCallback(dynamic value);

class GroupListWidget extends StatefulWidget {
  final ItemSelectedCallback onItemSelected;

  const GroupListWidget({Key key, this.onItemSelected}) : super(key: key);

  @override
  _GroupListWidgetState createState() => _GroupListWidgetState();
}

class _GroupListWidgetState extends State<GroupListWidget> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var group = Provider.of<List<Group>>(context);

    return group != null
        ? ListView(
            reverse: false,
            children: group.map((item) {
              return GroupItemWidget(item, () {
                SizeConfig.isLargeScreen ? widget.onItemSelected(item) :
                Navigator.of(context).pushNamed(NotificationsPageRoute,
                    arguments: Group(name: item.name, uid: item.uid, admin: item.admin));
              });
            }).toList(),
          )
        : Container();
  }
}
