import 'package:flutter/material.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: SvgPicture.asset(
          'assets/images/logo.svg',
          color: Palette.logoColor,
          width: 50.0 * SizeConfig.widthMultiplier,
          height: 12.5 * SizeConfig.heightMultiplier,
        ),
      ),
    );
  }
}
