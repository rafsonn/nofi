import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nofi/config/Palette.dart';

class NotificationItemWidget extends StatelessWidget {
  final snapshot;

  NotificationItemWidget(this.snapshot);

  @override
  Widget build(BuildContext context) {
    var locale = Localizations.localeOf(context).toString();
    return snapshot == null
        ? SizedBox()
        : Container(
            child: Column(
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Text(
                            snapshot.text,
                            style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                          ),
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Palette.gradientStartColor, Palette.gradientEndColor],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                stops: [0.0, 1.0],
                              ),
                              boxShadow: [
                                BoxShadow(
                                    color: Palette.shadowColor,
                                    offset: Offset(5.0, 5.0),
                                    blurRadius: 10.0)
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
                Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                  Container(
                    child: Text(
                      snapshot.timestamp == 0
                          ? "N/A"
                          : DateFormat.yMMMd(locale).add_jm().format(snapshot.timestamp.toDate()),
                      style: TextStyle(
                          color: Palette.captionColor, fontSize: 12.0, fontStyle: FontStyle.normal),
                    ),
                    margin: EdgeInsets.only(top: 10.0, bottom: 15.0),
                  )
                ])
              ],
            ),
          );
  }
}
