import 'package:flutter/material.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:provider/provider.dart';
import 'NotificationItemWidget.dart';
import 'package:nofi/models/Notification.dart' as prefix0;

class NotificationListWidget extends StatelessWidget {
  final ScrollController listScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    var notification = Provider.of<List<prefix0.Notification>>(context);

    return notification != null ? ListView(
      reverse: false,
      padding: EdgeInsets.all(2.4 * SizeConfig.imageSizeMultiplier),
      children: notification.map((item) {
        return NotificationItemWidget(item);
      }).toList(),
    ) : Container();
  }
}