import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/models/Validator.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoginWidget extends StatefulWidget {
  final dynamic loginPageKey;

  LoginWidget({Key key, this.loginPageKey}) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final GlobalKey<FormState> _loginKey = GlobalKey();
  final GlobalKey<FormState> _resetPasswordKey = GlobalKey();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final _validate = Validator();
  var _autoValidate = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserAuth>(context);

    return Container(
      padding: EdgeInsets.only(top: 1.6 * SizeConfig.heightMultiplier),
      decoration: BoxDecoration(
          color: Palette.whiteBackgroundColor,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(color: Palette.shadowColor, offset: Offset(5.0, 5.0), blurRadius: 10.0)
          ]),
      width: 90.0 * SizeConfig.widthMultiplier,
      height: 35.2 * SizeConfig.heightMultiplier,
      child: Center(
        child: SingleChildScrollView(
          child: Form(
            key: _loginKey,
            autovalidate: _autoValidate,
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              /// Email Input
              Container(
                  width: SizeConfig.screenWidth,
                  margin: EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: TextFormField(
                      controller: _emailController,
                      validator: _validate.email,
                      autovalidate: _autoValidate,
                      keyboardType: TextInputType.emailAddress,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.email, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate('email'),
                        hintStyle: TextStyle(color: Palette.textColor),
                      ))),

              /// Password Input
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      validator: _validate.isEmpty,
                      autovalidate: _autoValidate,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.vpn_key, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate('password'),
                        hintStyle: TextStyle(color: Palette.textColor),
                      ))),

              /// Login Button
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                      left: 10.0 * SizeConfig.widthMultiplier,
                      right: 10.0 * SizeConfig.widthMultiplier,
                      top: 3.8 * SizeConfig.heightMultiplier),
                  alignment: Alignment.center,
                  child: Row(children: <Widget>[
                    Expanded(
                      child: user.status == Status.Authenticating
                          ? SpinKitCircle(color: Palette.formButtonColor)
                          : FlatButton(
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                              color: Palette.formButtonColor,
                              onPressed: () async {
                                setState(() {
                                  _autoValidate = true;
                                });
                                if (_loginKey.currentState.validate()) {
                                  if (!await user.signIn(
                                      _emailController.text, _passwordController.text)) {
                                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                                    this.widget.loginPageKey.currentState.showSnackBar(SnackBar(
                                          content: Text(AppLocalizations.of(context)
                                              .translate('Something_goes_wrong')),
                                        ));
                                  }
                                }
                              },
                              child: Text(
                                AppLocalizations.of(context).translate('LogIn'),
                                style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                              ),
                            ),
                    ),
                  ])),

              /// Reset Password Button
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: Row(children: <Widget>[
                    Expanded(
                        child: FlatButton(
                            color: Colors.transparent,
                            onPressed: () => _resetPasswordDialog(context),
                            child: Text(
                              AppLocalizations.of(context).translate('Forgot_password'),
                              style: TextStyle(color: Palette.shadowColor.withOpacity(0.5)),
                            )))
                  ]))
            ]),
          ),
        ),
      ),
    );
  }

  _resetPasswordDialog(context) {
    final user = Provider.of<UserAuth>(context);
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
            title: Center(child: Text(AppLocalizations.of(context).translate('Password_reset'))),
            content: SingleChildScrollView(
              child: Form(
                key: _resetPasswordKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      controller: _emailController,
                      validator: _validate.email,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.mail, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate("email"),
                        hintStyle: TextStyle(color: Palette.formButtonColor),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(
                          left: 10.0 * SizeConfig.widthMultiplier,
                          right: 10.0 * SizeConfig.widthMultiplier,
                          top: 3.8 * SizeConfig.heightMultiplier),
                      alignment: Alignment.center,
                      child: FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 15.0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                        color: Palette.gradientEndColor,
                        onPressed: () async {
                          if (_resetPasswordKey.currentState.validate()) {
                            if (!await user.resetPassword(_emailController.text)) {
                              SystemChannels.textInput.invokeMethod('TextInput.hide');
                              this.widget.loginPageKey.currentState.showSnackBar(SnackBar(
                                    content: Text(AppLocalizations.of(context)
                                        .translate("Something_goes_wrong")),
                                  ));
                            } else
                              this.widget.loginPageKey.currentState.showSnackBar(SnackBar(
                                    content: Text(
                                      AppLocalizations.of(context).translate("Link_send"),
                                    ),
                                  ));
                            Navigator.pop(context);
                            _emailController.clear();
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context).translate('Reset'),
                          style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
