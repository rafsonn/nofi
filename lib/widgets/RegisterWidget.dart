import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/models/Validator.dart';
import 'package:provider/provider.dart';

class RegisterWidget extends StatefulWidget {
  final dynamic loginPageKey;

  RegisterWidget({Key key, this.loginPageKey}) : super(key: key);

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  final GlobalKey<FormState> _registerKey = GlobalKey();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmationController = TextEditingController();

  final _validate = Validator();
  var _autoValidate = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _passwordConfirmationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserAuth>(context);
    return Container(
      padding: EdgeInsets.only(top: 13.0),
      decoration: BoxDecoration(
          color: Palette.whiteBackgroundColor,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(color: Palette.shadowColor, offset: Offset(5.0, 5.0), blurRadius: 10.0)
          ]),
      width: 90.0 * SizeConfig.widthMultiplier,
      height: 38.0 * SizeConfig.heightMultiplier,
      child: Center(
        child: SingleChildScrollView(
          child: Form(
            key: _registerKey,
            autovalidate: _autoValidate,
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              /// Email Input
              Container(
                  width: SizeConfig.screenWidth,
                  margin: EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: TextFormField(
                      controller: _emailController,
                      validator: _validate.email,
                      autovalidate: _autoValidate,
                      keyboardType: TextInputType.emailAddress,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.email, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate("email"),
                        hintStyle: TextStyle(color: Palette.textColor),
                      ))),

              /// Password Input
              Container(
                  width: SizeConfig.screenWidth,
                  margin: EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      validator: _validate.password,
                      autovalidate: _autoValidate,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.vpn_key, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate('password'),
                        hintStyle: TextStyle(color: Palette.textColor),
                      ))),

              /// Confirm Password Input
              Container(
                  width: SizeConfig.screenWidth,
                  margin: EdgeInsets.symmetric(horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: TextFormField(
                      controller: _passwordConfirmationController,
                      obscureText: true,
                      validator: _validate.isEmpty,
                      autovalidate: _autoValidate,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.vpn_key, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate('confirm_password'),
                        hintStyle: TextStyle(color: Palette.textColor),
                      ))),

              /// Register Button
              Container(
                  width: SizeConfig.screenWidth,
                  margin: EdgeInsets.symmetric(
                      vertical: 3.8 * SizeConfig.heightMultiplier,
                      horizontal: 10.0 * SizeConfig.widthMultiplier),
                  alignment: Alignment.center,
                  child: Row(children: <Widget>[
                    Expanded(
                      child: user.status == Status.Authenticating
                          ? SpinKitCircle(color: Palette.formButtonColor)
                          : FlatButton(
                              padding:
                                  EdgeInsets.symmetric(vertical: 1.9 * SizeConfig.heightMultiplier),
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                              color: Palette.gradientEndColor,
                              onPressed: () async {
                                setState(() {
                                  _autoValidate = true;
                                });
                                if (_registerKey.currentState.validate()) {
                                  if (_passwordController.text ==
                                      _passwordConfirmationController.text) {
                                    if (!await user.signUp(
                                        _emailController.text, _passwordController.text)) {
                                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                                      this.widget.loginPageKey.currentState.showSnackBar(SnackBar(
                                            content: Text(AppLocalizations.of(context)
                                                .translate('Something_goes_wrong')),
                                          ));
                                    }
                                  } else {
                                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                                    this.widget.loginPageKey.currentState.showSnackBar(SnackBar(
                                          content: Text(AppLocalizations.of(context)
                                              .translate('Passwords_dont_match')),
                                        ));
                                  }
                                }
                              },
                              child: Text(
                                AppLocalizations.of(context).translate('Register'),
                                style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                              ),
                            ),
                    )
                  ])),
            ]),
          ),
        ),
      ),
    );
  }
}
