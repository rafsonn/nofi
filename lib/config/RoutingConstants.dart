const String MainPageRoute = '/';
const String LoginPageRoute = 'LoginPage';
const String GroupsPageRoute = 'GroupsPage';
const String NotificationsPageRoute = 'NotificationsPagePage';
const String SettingsPageRoute = 'SettingsPage';
const String TabletPageRoute = 'TabletPage';
