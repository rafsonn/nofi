import 'package:flutter/material.dart';

class Palette {
  static Color primaryColor = Color(0xff7a7dfa);
  static Color secondaryColor = Color(0xffb7a2e2);

  static Color primaryColor2 = Color(0xffa67dfa);
  static Color secondaryColor2 = Color(0xff7a7dfa);

  static Color logoColor = Colors.white;
  static Color backgroundColor = Color(0xfff8f9fc);
  static Color whiteBackgroundColor = Colors.white;

  static Color appbarColor = primaryColor;
  static Color buttonColor = secondaryColor;
  static Color formButtonColor = primaryColor;
  static Color avatarColor = secondaryColor;
  static Color inputColorLight = Colors.white;

  static Color selectedColor = primaryColor;
  static Color dividerColor = primaryColor;
  static Color underlineColor = primaryColor;
  static Color shadowColor = Colors.grey;

  static Color iconColor = primaryColor;
  static Color iconColorLight = Colors.white;
  static Color textColor = primaryColor;
  static Color textColorLight = Colors.white;
  static Color captionColor = Colors.black45;

  static Color deleteActionColor = Colors.red;
  static Color joinActionColor = Colors.deepOrangeAccent;
  static Color exitActionColor = Colors.purple;





  static Color gradientStartColor = Color(0xffa67dfa);
  static Color gradientEndColor = Color(0xff7a7dfa);

  static LinearGradient backgroundGradient = LinearGradient(
      begin: Alignment.center,
      end: Alignment.bottomRight,
      colors: [Palette.gradientStartColor, Palette.gradientEndColor]);
}