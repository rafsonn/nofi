import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:nofi/models/Group.dart';
import 'package:nofi/models/Notification.dart' as prefix0;
import 'dart:async';

class DatabaseService {
  final Firestore _db = Firestore.instance;

//  Groups

  Stream<List<Group>> streamAllGroups() {
    var ref = _db.collection('groups').orderBy('timestamp', descending: true);

    return ref
        .snapshots()
        .map((list) => list.documents.map((doc) => Group.fromFirestore(doc)).toList());
  }

  Stream<List<Group>> streamMyGroups(FirebaseUser user) {
    var ref = _db
        .collection('groups')
        .where("admin", isEqualTo: user.uid)
        .orderBy('timestamp', descending: true);

    return ref
        .snapshots()
        .map((list) => list.documents.map((doc) => Group.fromFirestore(doc)).toList());
  }

  Stream<List<Group>> streamJoinedGroups(FirebaseUser user) {
    var ref = _db
        .collection('groups')
        .where("members", arrayContains: user.uid)
        .orderBy('timestamp', descending: true);

    return ref
        .snapshots()
        .map((list) => list.documents.map((doc) => Group.fromFirestore(doc)).toList());
  }

  Future<void> addGroup(dynamic group) {
    var ref = _db.collection('groups');

    return ref.add(group);
  }

  Future<void> removeGroup(String groupUID) {
    var ref = _db.collection('groups');

    return ref.document(groupUID).delete();
  }

  Future<void> joinGroup(String groupUID, FirebaseUser user) {
    var ref = _db.collection('groups').document(groupUID);

    return ref.updateData({
      'members': FieldValue.arrayUnion([user.uid])
    });
  }

  Future<void> exitGroup(String groupUID, FirebaseUser user) {
    var ref = _db.collection('groups').document(groupUID);

    return ref.updateData({
      'members': FieldValue.arrayRemove([user.uid])
    });
  }

  Future<void> updateGroup(dynamic group) {
    var ref = _db.collection('groups').document(group['uid']);

    return ref.updateData(
        {'lastNotification': group['lastNotification'], 'timestamp': group['timestamp']});
  }

//  Notifications

  Stream<List<prefix0.Notification>> streamNotifications(String groupUID) {
    var ref =
        _db.collection('messages').where("groupUID", isEqualTo: groupUID).orderBy('timestamp');

    return ref.snapshots().map(
        (list) => list.documents.map((doc) => prefix0.Notification.fromFirestore(doc)).toList());
  }

  Future<void> sendNotification(dynamic notification) {
    var ref = _db.collection('messages');

    return ref.add(notification);
  }
}
