import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nofi/buttons/SwitchToLoginButton.dart';
import 'package:nofi/buttons/SwitchToRegisterButton.dart';
import 'package:nofi/widgets/LoginWidget.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/widgets/Logo.dart';
import 'package:nofi/widgets/RegisterWidget.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _loginPageKey = GlobalKey();

  double _keyboardMultiplier = 1.0;

  static const _animationDuration = Duration(milliseconds: 1250);

  Widget _currentForm;
  Widget _currentButton;

  _button1() => SwitchToRegisterButton(onPressed: () {
        setState(() {
          _currentForm = _form2();
          _currentButton = _button2();
        });
      });

  _button2() => SwitchToLoginButton(onPressed: () {
        setState(() {
          _currentForm = _form1();
          _currentButton = _button1();
        });
      });

  _form1() => LoginWidget(loginPageKey: _loginPageKey);
  _form2() => RegisterWidget(loginPageKey: _loginPageKey);

  @override
  void initState() {
    super.initState();
    setState(() {
      _currentForm = _form1();
      _currentButton = _button1();
    });

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        visible
            ? setState(() {
                _keyboardMultiplier = 0.4;
              })
            : setState(() {
                _keyboardMultiplier = 1.0;
              });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: null, //user to override the back button press
        child: Scaffold(
          key: _loginPageKey,
          resizeToAvoidBottomPadding: false,
          //  avoids the bottom overflow warning when keyboard is shown
          body: Container(
            height: SizeConfig.screenHeight,
              decoration: BoxDecoration(gradient: Palette.backgroundGradient),
              child: SingleChildScrollView(
                child: Column(children: <Widget>[
                  AnimatedSize(
                    child:
                        SizedBox(height: SizeConfig.heightMultiplier * 12.5 * _keyboardMultiplier),
                    vsync: this,
                    duration: Duration(milliseconds: 350),
                  ),
                  Logo(),
                  AnimatedSize(
                    child: SizedBox(height: SizeConfig.screenHeight * 0.125 * _keyboardMultiplier),
                    vsync: this,
                    duration: Duration(milliseconds: 150),
                  ),
                  AnimatedSwitcher(
                      duration: _animationDuration,
                      switchInCurve: Interval(
                        0.2, //begin
                        0.8, //end
                        curve: Curves.linear,
                      ),
                      switchOutCurve: Interval(
                        0.2, //begin
                        0.8, //end
                        curve: Curves.linear,
                      ),
                      transitionBuilder: (child, animation) => SlideTransition(
                            position: Tween<Offset>(begin: Offset(0.0, 4.0), end: Offset.zero)
                                .animate(animation),
                            child: child,
                          ),
                      child: _currentForm),
                  AnimatedSwitcher(
                    duration: _animationDuration,
                    switchInCurve: Interval(0.8, 1.0, curve: Curves.easeInExpo),
                    switchOutCurve: Interval(
                      0.0,
                      1.0,
                      curve: Curves.easeInExpo,
                    ),
                    child: _currentButton,
                  )
                ]),
              )),
        ));
  }
}
