import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/pages/GroupsPage.dart';
import 'package:nofi/pages/LoginPage.dart';
import 'package:nofi/pages/TabletPage.dart';
import 'package:nofi/widgets/SplashWidget.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => UserAuth.instance(),
      child: Consumer(
        builder: (context, UserAuth user, _) {
            switch (user.status) {
              case Status.Uninitialized:
              return SplashWidget();
              case Status.Unauthenticated:
                return LoginPage();
              case Status.Authenticating:
                return LoginPage();
              case Status.Authenticated:
                return SizeConfig.isLargeScreen && !SizeConfig.isPortrait ? TabletPage() : GroupsPage();
          }
        },
      ),
    );
  }
}
