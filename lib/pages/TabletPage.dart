import 'package:flutter/material.dart';
import 'package:nofi/pages/GroupsPage.dart';
import 'package:nofi/pages/NotificationsPage.dart';

class TabletPage extends StatefulWidget {
  @override
  _TabletPageState createState() => _TabletPageState();
}

class _TabletPageState extends State<TabletPage> {
  var selectedGroup;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: GroupsPage(
            onItemSelected: (value) {
              selectedGroup = value;
              setState(() {});
            },
          ),
        ),
        Expanded(child: NotificationsPage(group: selectedGroup))
      ],
    );
  }
}
