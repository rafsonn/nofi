import 'package:flutter/material.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/widgets/SettingsListWidget.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(right: 12.0 * SizeConfig.widthMultiplier),
          child: Center(child: Text(AppLocalizations.of(context).translate("Settings"))),
        ),
      ),
      backgroundColor: Palette.backgroundColor,
      body: SafeArea(
        child: ChangeNotifierProvider(
          create: (_) => UserAuth.instance(),
          child: Consumer(
            builder: (context, UserAuth user, _) {
              return SettingsListWidget();
            },
          ),
        ),
      ),
    );
  }
}
