import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nofi/config/DatabaseService%20.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/widgets/NotificationListWidget.dart';
import 'package:provider/provider.dart';
import 'package:nofi/models/Notification.dart' as prefix0;

class NotificationsPage extends StatefulWidget {
  final dynamic group;

  NotificationsPage({Key key, this.group}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  final TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final db = DatabaseService();

    return widget.group == null
        ? Scaffold(
            appBar: AppBar(),
            body: Container(
              color: Palette.backgroundColor,
              child: Center(
                  child: Text(
                AppLocalizations.of(context).translate("Choose_group"),
                style: TextStyle(fontSize: 2.5 * SizeConfig.textMultiplier),
              )),
            ))
        : Scaffold(
            appBar: AppBar(
              title: Padding(
                padding: EdgeInsets.only(right: 12.0 * SizeConfig.widthMultiplier),
                child: Center(child: Text(widget.group.name)),
              ),
              centerTitle: true,
            ),
            backgroundColor: Palette.backgroundColor,
            body: SafeArea(
              child: ChangeNotifierProvider(
                create: (_) => UserAuth.instance(),
                child: Consumer(
                  builder: (context, UserAuth user, _) {
                    return Column(
                      children: <Widget>[
                        StreamProvider<List<prefix0.Notification>>.value(
                          value: db.streamNotifications(widget.group.uid),
                          child: Flexible(
                            child: NotificationListWidget(),
                          ),
                        ),
                        Divider(
                          height: 1.0,
                        ),
                        widget.group.admin == user.user.uid
                            ? Container(
                                color: Palette.inputColorLight,
                                child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Row(
                                    children: <Widget>[
                                      Flexible(
                                        child: TextFormField(
                                          controller: _textController,
                                          decoration: InputDecoration.collapsed(
                                              hintText: AppLocalizations.of(context)
                                                  .translate("Text_message"),
                                              hintStyle: TextStyle(color: Palette.captionColor)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 4.0),
                                        child: IconButton(
                                          icon: Icon(Icons.send),
                                          color: Palette.formButtonColor,
                                          onPressed: () async {
                                            final timestamp = DateTime.now();
                                            final text = _textController.text;
                                            Future.delayed(Duration.zero, () {
                                              _textController.clear();
                                            });
                                            if (_textController.text.isNotEmpty) {
                                              db.sendNotification({
                                                "groupUID": widget.group.uid,
                                                "text": text,
                                                "timestamp": timestamp,
                                                "userUID": widget.group.admin
                                              });
                                              db.updateGroup({
                                                "uid": widget.group.uid,
                                                "lastNotification": text,
                                                "timestamp": timestamp,
                                              });
                                            }
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            : Container()
                      ],
                    );
                  },
                ),
              ),
            ),
          );
  }
}
