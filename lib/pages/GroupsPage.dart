import 'package:flutter/material.dart';
import 'package:nofi/config/DatabaseService%20.dart';
import 'package:nofi/config/Palette.dart';
import 'package:nofi/config/RoutingConstants.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';
import 'package:nofi/models/Group.dart';
import 'package:nofi/models/UserAuth.dart';
import 'package:nofi/models/Validator.dart';
import 'package:nofi/widgets/GroupListWidget.dart';
import 'package:provider/provider.dart';

typedef Null ItemSelectedCallback(dynamic value);

class GroupsPage extends StatefulWidget {
  final ItemSelectedCallback onItemSelected;

  const GroupsPage({Key key, this.onItemSelected}) : super(key: key);

  @override
  _GroupsPageState createState() => _GroupsPageState();
}

class _GroupsPageState extends State<GroupsPage> with SingleTickerProviderStateMixin {
  TabController _tabController;

  final GlobalKey<FormState> _addGroupKey = GlobalKey();
  final TextEditingController _nameController = TextEditingController();

  final _validator = Validator();

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserAuth>(context);
    final db = DatabaseService();

    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(AppLocalizations.of(context).translate('Groups'))),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        mini: false,
        backgroundColor: Palette.buttonColor,
        child: const Icon(Icons.add),
        onPressed: () => {_addGroupDialog(context, db)},
      ),
      backgroundColor: Palette.backgroundColor,
      body: SafeArea(
        child: TabBarView(
          children: <Widget>[
            Container(
              child: Stack(
                alignment: Alignment.topLeft,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      StreamProvider<List<Group>>.value(
                          value: db.streamMyGroups(user.user),
                          child: Flexible(child: GroupListWidget(onItemSelected: (value) {
                            widget.onItemSelected(value);
                            setState(() {});
                          })))
                    ],
                  ),
                ],
              ),
            ),
            Container(
              child: Stack(
                alignment: Alignment.topLeft,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      StreamProvider<List<Group>>.value(
                          value: db.streamJoinedGroups(user.user),
                          child: Flexible(child: GroupListWidget(onItemSelected: (value) {
                            widget.onItemSelected(value);
                            setState(() {});
                          })))
                    ],
                  ),
                ],
              ),
            ),
            Container(
              child: Stack(
                alignment: Alignment.topLeft,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      StreamProvider<List<Group>>.value(
                          value: db.streamAllGroups(),
                          child: Flexible(child: GroupListWidget(onItemSelected: (value) {
                            widget.onItemSelected(value);
                            setState(() {});
                          })))
                    ],
                  ),
                ],
              ),
            ),
          ],
          controller: _tabController,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        notchMargin: 3.5,
        shape: CircularNotchedRectangle(),
        color: Palette.appbarColor,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.menu,
                color: Palette.iconColorLight,
              ),
              onPressed: () {
                _showLeftModal();
              },
            ),
            Expanded(child: SizedBox()),
            IconButton(
              icon: Icon(
                Icons.more_vert,
                color: Palette.iconColorLight,
              ),
              onPressed: () {
                _showRightModal();
              },
            )
          ],
        ),
      ),
    );
  }

  _showLeftModal() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Ink(
                color: _tabController.index == 0 ? Palette.selectedColor : null,
                child: ListTile(
                  selected: _tabController.index == 0 ? true : false,
                  leading: Icon(Icons.group),
                  title: Text(AppLocalizations.of(context).translate('My groups')),
                  onTap: () {
                    Navigator.pop(context);
                    _tabController.animateTo(0);
                  },
                ),
              ),
              Ink(
                color: _tabController.index == 1 ? Palette.selectedColor : null,
                child: ListTile(
                  selected: _tabController.index == 1 ? true : false,
                  leading: Icon(Icons.group),
                  title: Text(AppLocalizations.of(context).translate('Joined groups')),
                  onTap: () {
                    Navigator.pop(context);
                    _tabController.animateTo(1);
                  },
                ),
              ),
              Ink(
                color: _tabController.index == 2 ? Palette.selectedColor : null,
                child: ListTile(
                  selected: _tabController.index == 2 ? true : false,
                  leading: Icon(Icons.group),
                  title: Text(AppLocalizations.of(context).translate('All groups')),
                  onTap: () {
                    Navigator.pop(context);
                    _tabController.animateTo(2);
                  },
                ),
              ),
            ],
          );
        });
  }

  _showRightModal() {
    final user = Provider.of<UserAuth>(context);

    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.settings),
                title: Text(AppLocalizations.of(context).translate('Settings')),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed(SettingsPageRoute);
                },
              ),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text(AppLocalizations.of(context).translate('Logout')),
                onTap: () async {
                  Navigator.pop(context);
                  user.signOut();
                },
              ),
            ],
          );
        });
  }

  _addGroupDialog(context, DatabaseService db) {
    final user = Provider.of<UserAuth>(context);
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
            title: Center(child: Text(AppLocalizations.of(context).translate("Create group"))),
            content: SingleChildScrollView(
              child: Form(
                key: _addGroupKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      controller: _nameController,
                      validator: _validator.isEmpty,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.75)),
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Palette.underlineColor, width: 0.5)),
                        prefixIcon: Icon(Icons.group, color: Palette.iconColor),
                        hintText: AppLocalizations.of(context).translate("group_name"),
                        hintStyle: TextStyle(color: Palette.formButtonColor),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(
                          left: 10.0 * SizeConfig.widthMultiplier,
                          right: 10.0 * SizeConfig.widthMultiplier,
                          top: 3.8 * SizeConfig.heightMultiplier),
                      alignment: Alignment.center,
                      child: FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 1.9 * SizeConfig.heightMultiplier),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                        color: Palette.formButtonColor,
                        onPressed: () async {
                          if (_addGroupKey.currentState.validate() && (_nameController.text.length < 15)) {
                            if (db.addGroup({
                                  'name': _nameController.text,
                                  'admin': user.user.uid,
                                  'timestamp': DateTime.now(),
                                  'lastNotification': ' '
                                }) !=
                                null) {
                              Navigator.pop(context);
                              _nameController.clear();
                            }
                          } else {
                            print('object');
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context).translate('Create'),
                          style: TextStyle(color: Palette.textColorLight, fontSize: 16),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
