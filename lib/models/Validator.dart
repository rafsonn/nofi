class Validator {
  final _emailRegex = r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  final _passwordRegex = r"(?=.{6,32})(?=.*[A-Z])(?=.*[a-z])(?=.*\d)";

  get email => _validateEmail;

  get password => _validatePassword;

  get isEmpty => _validateIsEmpty;

  String _validateIsEmpty(String value) => value.length == 0 ? "Required" : null;

  String _validateEmail(String value) {
    RegExp regExp = RegExp(_emailRegex);
    if (value.length == 0) {
      return 'Required';
    }

    if (!regExp.hasMatch(value)) {
      return 'Email is not valid';
    }
    return null;
  }

  String _validatePassword(String value) {
    RegExp regExp = RegExp(_passwordRegex);
    if (value.length == 0) {
      return 'Required';
    }

    if (!regExp.hasMatch(value)) {
      return 'Required:   6-32 chars, {[0-9], [A-Z], [a-z]}';
    }
    return null;
  }
}
