import 'package:flutter/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserAuth with ChangeNotifier {
  FirebaseAuth _auth;
  FirebaseUser _user;

  Status _status = Status.Uninitialized;

  UserAuth.instance() : _auth = FirebaseAuth.instance {
    _auth.onAuthStateChanged.listen(_onAuthStateChanged);
  }

  Status get status => _status;

  FirebaseUser get user => _user;

  Future signUp(String email, String password) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      await _auth.createUserWithEmailAndPassword(email: email, password: password);
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future signIn(String email, String password) async {
    try {
      _status = Status.Authenticating;
      notifyListeners();
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      print(_auth.currentUser());
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future signOut() async {
    _auth.signOut();
    _status = Status.Unauthenticated;
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  Future resetPassword(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future changeEmail(String email) async {
    try {
      await _user.updateEmail(email);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future changePassword(String password) async {
    try {
      await _user.updatePassword(password);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future reauthenticate(FirebaseUser user, String password) async {
    try {
      final AuthCredential credential =
          EmailAuthProvider.getCredential(email: user.email, password: password);
      await _user.reauthenticateWithCredential(credential);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future _onAuthStateChanged(FirebaseUser firebaseUser) async {
    if (firebaseUser == null) {
      _status = Status.Unauthenticated;
    } else {
      _user = firebaseUser;
      _status = Status.Authenticated;
    }
    notifyListeners();
  }
}

enum Status { Uninitialized, Unauthenticated, Authenticating, Authenticated }
