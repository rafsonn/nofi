import 'package:cloud_firestore/cloud_firestore.dart';

class Group {
  final uid;
  final name;
  final admin;
  final members;
  final lastNotification;
  final timestamp;

  Group({
    this.uid,
    this.name,
    this.admin,
    this.members,
    this.lastNotification,
    this.timestamp
  });

  factory Group.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Group(
        uid: doc.documentID,
        name: data['name'] ?? 'N/A',
        admin: data['admin'] ?? 'N/A',
        members: data['members'] ?? '',
        lastNotification: data['lastNotification'] ?? 'N/A',
        timestamp: data['timestamp'] ?? 0);
  }
}
