import 'package:cloud_firestore/cloud_firestore.dart';

class Notification{
  final uid;
  final groupUID;
  final text;
  final timestamp;
  final userUID;

  Notification({
    this.uid,
    this.groupUID,
    this.userUID,
    this.text,
    this.timestamp
  });

  factory Notification.fromFirestore(DocumentSnapshot doc){
    Map data = doc.data;
    return Notification(
      uid: doc.documentID,
      groupUID: data['groupUID'] ?? '',
      userUID: data['userUID'] ?? '',
      text: data['text'] ?? '',
      timestamp: data['timestamp'] ?? ''
    );
  }
}