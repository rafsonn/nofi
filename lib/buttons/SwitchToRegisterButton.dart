import 'package:flutter/material.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';

class SwitchToRegisterButton extends StatelessWidget {
  final Function onPressed;

  const SwitchToRegisterButton({Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 0.5 * SizeConfig.heightMultiplier),
        child: FlatButton(
            color: Colors.transparent,
            onPressed: this.onPressed,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('Dont_have_account') + '  ',
                style: TextStyle(color: Colors.black.withOpacity(0.5),fontSize: 2.0 * SizeConfig.textMultiplier),
              ),
              Text(AppLocalizations.of(context).translate('Register'),
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold,fontSize: 2.0 * SizeConfig.textMultiplier))
            ])));
  }
}
