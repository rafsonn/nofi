import 'package:flutter/material.dart';
import 'package:nofi/config/SizeConfig.dart';
import 'package:nofi/models/AppLocalizations.dart';

class SwitchToLoginButton extends StatelessWidget {
  final Function onPressed;

  const SwitchToLoginButton({Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 0.5 * SizeConfig.heightMultiplier),
        margin: EdgeInsets.symmetric(horizontal: 20.0 * SizeConfig.widthMultiplier),
        child: FlatButton(
            color: Colors.transparent,
            onPressed: this.onPressed,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('Back_to') + ' ',
                style: TextStyle(color: Colors.black.withOpacity(0.5), fontSize: 2.0 * SizeConfig.textMultiplier),
              ),
              Text(
                AppLocalizations.of(context).translate('_LogIn'),
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 2.0 * SizeConfig.textMultiplier)),
            ])));
  }
}
